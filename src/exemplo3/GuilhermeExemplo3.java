/**
 * ArrayWriter: Sincroniizacao de threads
 * Exemplo que mostra um problema na
 * escrita compartilhada de um vetor
 * por multiplas threads.
 * 
 * Autor: Guilherme
 * Ultima modificacao: 09/08/2017
 */
package exemplo3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class GuilhermeExemplo3  {

    
    public static void main(String [] args){
        
        GuilhermeVetorCompartilhado v = new GuilhermeVetorCompartilhado(6);
        
        GuilhermeArrayWriter w1 = new GuilhermeArrayWriter(1, v);
        GuilhermeArrayWriter w2 = new GuilhermeArrayWriter(11, v);
        
        ExecutorService executor = Executors.newCachedThreadPool();
        
        executor.execute(w1);
        executor.execute(w2);
        
        //A partir daqui nao aceita mais threads
        executor.shutdown();
        
        //Mostra o conteudo do vetorCompartilhado apos as threads finalizarem
        try {
            //Espera 1 minuto para que todas as threads finalizem
            boolean tasksEnded = executor.awaitTermination(1, TimeUnit.MINUTES);
            
            if (tasksEnded)
                System.out.println(v);
            else
                System.out.println("Timeout!");            
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        
        
        v.sub(2, 0);
        v.sub(0, -1);
        
        System.out.println(v);           
    }//fim main
    
}//fim classe
